import { EventEmitter } from "@pixi/utils";
import { token } from "brandi";
import { Ticker } from "pixi.js";
import { CallbackCollector } from "src/utils/CallbackCollector";

export const UTILS_TOKENS = {
  callbacksCollector: token<CallbackCollector>("callbacksCollector"),
  eventEmitter: token<EventEmitter>("EventEmitter"),
  ticker: token<Ticker>("ticker"),
};
