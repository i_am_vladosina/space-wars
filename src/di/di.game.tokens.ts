import { Factory, token } from "brandi";
import { AppOptions } from "src/core/GameBaseApplication";
import { GameEventsBridge } from "src/core/GameEventsBridge";
import { GameCoreEvents } from "src/core/games-core.types";
import { AssetsLoader } from "src/game/AssetsLoader";
import { GameApplication } from "src/game/GameApplication";
import { World } from "src/game/World";
import { SpaceShip } from "src/game/game-objects/SpaceShip/SpaceShip";
import { SpaceShipViewComponent } from "src/game/game-objects/SpaceShip/SpaceShipViewComponent";
import { GameObjectsSpawnerScript } from "src/game/scripts/GameObjectsSpawnerScript";
import { StartUpScript } from "src/game/scripts/StartUpScript";

export const GAME_TOKENS = {
  gameApplication: token<GameApplication>("gameApplication"),
  gameAppOptions: token<AppOptions>("gameAppOptions"),
  gameWorld: token<World>("gameWorld"),
  gameStartUpScript: token<StartUpScript>("gameStartUpScript"),
  gameObjectsSpawnerScript: token<GameObjectsSpawnerScript>("gameObjectsSpawnerScript"),
  gameAssetsLoader: token<AssetsLoader>("gameAssetsLoader"),
  gameSpaceShipViewComponent: token<SpaceShipViewComponent>("gameSpaceShipViewComponent"),
  gameSpaceShip: token<Factory<SpaceShip>>("gameSpaceShip"),
};
