import { Container } from "brandi";
import { initGameDeps } from "./di.game";
import { initCoreGameDeps } from "./di.games-common";
import { initUtilsDeps } from "./di.utils";

export function createDiRootContainer(): Container {
  const container = new Container();

  initCoreGameDeps(container);
  initGameDeps(container);
  initUtilsDeps(container);

  return container;
}
