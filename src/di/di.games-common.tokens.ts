import { EventEmitter } from "@pixi/utils";
import { Factory, token } from "brandi";
import { Ticker } from "pixi.js";
import { GameEventsBridge } from "src/core/GameEventsBridge";
import { GameLoop } from "src/core/GameLoop";
import { GameObject } from "src/core/GameObject";
import { GameTime } from "src/core/GameTime";
import { CameraComponent } from "src/core/common-components/CameraComponent";
import { CountdownComponent } from "src/core/common-components/CountdownComponent";
import { HealthComponent } from "src/core/common-components/HealthComponent";
import { LinearMovementComponent } from "src/core/common-components/MovementComponent";
import { TimeStopwatchComponent } from "src/core/common-components/StopwatchComponent";
import { TransformComponent } from "src/core/common-components/TransformComponent";
import { GameCoreEvents } from "src/core/games-core.types";

export const GAMES_CORE_TOKENS = {
  gameLoop: token<GameLoop>("gameLoop"),
  gameTime: token<GameTime>("gameTime"),
  ticker: token<Ticker>("ticker"),
  eventEmitter: token<EventEmitter>("eventEmitter"),
  cameraComponent: token<CameraComponent>("cameraComponent"),
  transformComponent: token<TransformComponent>("transformComponent"),
  linearMovementComponent: token<LinearMovementComponent>("linearMovementComponent"),
  healthComponent: token<HealthComponent>("healthComponent"),
  timeStopwatchComponent: token<TimeStopwatchComponent>("timeStopwatchComponent"),
  countdownComponent: token<CountdownComponent>("countdownComponent"),
  gameObjectFactory: token<Factory<GameObject>>("gameObjectFactory"),
  gameEventsBridge: token<GameEventsBridge<GameCoreEvents>>("gameEventsBridge"),
};
