import { GAME_TOKENS } from "./di.game.tokens";
import { GAMES_CORE_TOKENS } from "./di.games-common.tokens";
import { UTILS_TOKENS } from "./di.utils.tokens";

export const DI_TOKENS = {
  ...GAME_TOKENS,
  ...GAMES_CORE_TOKENS,
  ...UTILS_TOKENS,
};
