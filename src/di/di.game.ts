import { Container } from "brandi";
import { GameEventsBridge } from "src/core/GameEventsBridge";
import { AssetsLoader } from "src/game/AssetsLoader";
import { GameApplication } from "src/game/GameApplication";
import { World } from "src/game/World";
import { SpaceShip } from "src/game/game-objects/SpaceShip/SpaceShip";
import { SpaceShipViewComponent } from "src/game/game-objects/SpaceShip/SpaceShipViewComponent";
import { GAME_APP_OPTIONS } from "src/game/game.constants";
import { GameObjectsSpawnerScript } from "src/game/scripts/GameObjectsSpawnerScript";
import { StartUpScript } from "src/game/scripts/StartUpScript";
import { GAME_TOKENS } from "./di.game.tokens";

export function initGameDeps(container: Container): void {
  container.bind(GAME_TOKENS.gameAppOptions).toConstant(GAME_APP_OPTIONS);
  container.bind(GAME_TOKENS.gameApplication).toInstance(GameApplication).inSingletonScope();
  container.bind(GAME_TOKENS.gameWorld).toInstance(World).inTransientScope();
  container.bind(GAME_TOKENS.gameStartUpScript).toInstance(StartUpScript).inTransientScope();
  container.bind(GAME_TOKENS.gameObjectsSpawnerScript).toInstance(GameObjectsSpawnerScript).inTransientScope();
  container.bind(GAME_TOKENS.gameAssetsLoader).toInstance(AssetsLoader).inSingletonScope();
  container.bind(GAME_TOKENS.gameSpaceShip).toFactory(SpaceShip);
  container.bind(GAME_TOKENS.gameSpaceShipViewComponent).toInstance(SpaceShipViewComponent).inTransientScope();
}
