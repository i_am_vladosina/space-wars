import { createInjectionHooks } from "brandi-react";
import { DI_TOKENS } from "src/di/di.tokens";

const [useGameEventsBridge, useAssetsLoader, useGameApplication] = createInjectionHooks(
  DI_TOKENS.gameEventsBridge,
  DI_TOKENS.gameAssetsLoader,
  DI_TOKENS.gameApplication
);

export { useGameEventsBridge, useAssetsLoader, useGameApplication };
