import { EventEmitter } from "@pixi/utils";
import { Container } from "brandi";
import { Ticker } from "pixi.js";
import { CallbackCollector } from "src/utils/CallbackCollector";
import { UTILS_TOKENS } from "./di.utils.tokens";

export function initUtilsDeps(container: Container): void {
  container.bind(UTILS_TOKENS.callbacksCollector).toInstance(CallbackCollector).inTransientScope();
  container.bind(UTILS_TOKENS.eventEmitter).toInstance(EventEmitter).inTransientScope();
  container
    .bind(UTILS_TOKENS.ticker)
    .toInstance(() => Ticker.shared)
    .inSingletonScope();
}
