import { EventEmitter } from "@pixi/utils";
import { Container } from "brandi";
import { Ticker } from "pixi.js";
import { GameEventsBridge } from "src/core/GameEventsBridge";
import { GameLoop } from "src/core/GameLoop";
import { GameObject } from "src/core/GameObject";
import { GameTime } from "src/core/GameTime";
import { CameraComponent } from "src/core/common-components/CameraComponent";
import { CountdownComponent } from "src/core/common-components/CountdownComponent";
import { HealthComponent } from "src/core/common-components/HealthComponent";
import { LinearMovementComponent } from "src/core/common-components/MovementComponent";
import { TimeStopwatchComponent } from "src/core/common-components/StopwatchComponent";
import { TransformComponent } from "src/core/common-components/TransformComponent";
import { GAMES_CORE_TOKENS } from "./di.games-common.tokens";

export function initCoreGameDeps(container: Container): void {
  container.bind(GAMES_CORE_TOKENS.gameLoop).toInstance(GameLoop).inTransientScope();
  container.bind(GAMES_CORE_TOKENS.gameTime).toInstance(GameTime).inTransientScope();
  container
    .bind(GAMES_CORE_TOKENS.ticker)
    .toInstance(() => Ticker.shared)
    .inSingletonScope();
  container.bind(GAMES_CORE_TOKENS.eventEmitter).toInstance(EventEmitter).inTransientScope();
  container.bind(GAMES_CORE_TOKENS.gameObjectFactory).toFactory(GameObject);
  container.bind(GAMES_CORE_TOKENS.cameraComponent).toInstance(CameraComponent).inTransientScope();
  container.bind(GAMES_CORE_TOKENS.transformComponent).toInstance(TransformComponent).inTransientScope();
  container.bind(GAMES_CORE_TOKENS.linearMovementComponent).toInstance(LinearMovementComponent).inTransientScope();
  container.bind(GAMES_CORE_TOKENS.healthComponent).toInstance(HealthComponent).inTransientScope();
  container.bind(GAMES_CORE_TOKENS.timeStopwatchComponent).toInstance(TimeStopwatchComponent).inTransientScope();
  container.bind(GAMES_CORE_TOKENS.countdownComponent).toInstance(CountdownComponent).inTransientScope();
  container.bind(GAMES_CORE_TOKENS.gameEventsBridge).toInstance(GameEventsBridge).inSingletonScope();
}
