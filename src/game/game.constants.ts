import { Color } from "pixi.js";
import { AppOptions } from "src/core/GameBaseApplication";

export const GAME_APP_OPTIONS: AppOptions = {
  pixiApp: {
    backgroundAlpha: 0,
    backgroundColor: new Color(0xff0000),
  },
};
