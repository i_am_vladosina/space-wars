import { injected } from "brandi";
import { BaseWorld } from "src/core/BaseWorld";
import { GameTime } from "src/core/GameTime";
import { GAME_TOKENS } from "src/di/di.game.tokens";
import { GAMES_CORE_TOKENS } from "src/di/di.games-common.tokens";
import { DI_TOKENS } from "src/di/di.tokens";
import { GameApplication } from "./GameApplication";
import { GameObjectsSpawnerScript } from "./scripts/GameObjectsSpawnerScript";
import { StartUpScript } from "./scripts/StartUpScript";

export class World extends BaseWorld {
  constructor(
    public readonly gameTime: GameTime,
    private readonly startUpScript: StartUpScript,
    private readonly spawnerScript: GameObjectsSpawnerScript
  ) {
    super(gameTime);
  }

  public init(gameApp: GameApplication, ...args: any): void {
    super.init(gameApp);

    this.addScript(this.spawnerScript, this.startUpScript);

    gameApp.gameLoop.start();
  }
}

injected(World, GAMES_CORE_TOKENS.gameTime, GAME_TOKENS.gameStartUpScript, DI_TOKENS.gameObjectsSpawnerScript);
