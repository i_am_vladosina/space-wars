import { injected } from "brandi";
import { GameBaseApplication } from "src/core/GameBaseApplication";
import { GAME_TOKENS } from "src/di/di.game.tokens";
import { GAMES_CORE_TOKENS } from "src/di/di.games-common.tokens";
import { UTILS_TOKENS } from "src/di/di.utils.tokens";
import { World } from "./World";

export class GameApplication extends GameBaseApplication<World> {}

injected(
  GameApplication,
  GAME_TOKENS.gameAppOptions,
  GAME_TOKENS.gameWorld,
  GAMES_CORE_TOKENS.gameEventsBridge,
  UTILS_TOKENS.callbacksCollector,
  GAMES_CORE_TOKENS.gameLoop
);
