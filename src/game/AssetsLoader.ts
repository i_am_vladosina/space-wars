import { Assets, Spritesheet, Texture } from "pixi.js";
import spaceShipImage from "src/assets/spaceship.png";

export interface AssetsBundle {
  spaceShip: Texture;
}

export class AssetsLoader {
  private readonly bundleConfig: Record<keyof AssetsBundle, string> = {
    spaceShip: spaceShipImage,
  };
  private readonly bundleName = "main-bundle";
  public bundle: AssetsBundle;

  constructor() {
    Assets.addBundle(this.bundleName, this.bundleConfig);
  }

  public async loadBundle(): Promise<AssetsBundle> {
    const bundle: AssetsBundle = await Assets.loadBundle(this.bundleName);
    this.bundle = bundle;
    return bundle;
  }
}
