import { injected } from "brandi";
import { BaseScript } from "src/core/BaseScript";
import { GAME_TOKENS } from "src/di/di.game.tokens";
import { SpaceShip } from "../game-objects/SpaceShip/SpaceShip";

export class StartUpScript extends BaseScript {
  constructor(private readonly spaceShipFactory: () => SpaceShip) {
    super();
  }

  public start(): void {
    console.log("StartUpScript");
    this.world.addToWorld(this.spaceShipFactory());
  }
}

injected(StartUpScript, GAME_TOKENS.gameSpaceShip);
