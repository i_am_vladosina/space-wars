import { injected } from "brandi";
import { Sprite } from "pixi.js";
import { ViewComponent } from "src/core/common-components/ViewComponent";
import { GAME_TOKENS } from "src/di/di.game.tokens";
import { AssetsLoader } from "src/game/AssetsLoader";

export class SpaceShipViewComponent extends ViewComponent<Sprite> {
  constructor(private readonly assetsLoader: AssetsLoader) {
    super();
  }

  public createView(): Sprite {
    const sprite = new Sprite(this.assetsLoader.bundle.spaceShip);
    sprite.scale.set(0.3);
    return sprite;
  }
}

injected(SpaceShipViewComponent, GAME_TOKENS.gameAssetsLoader);
