import { injected } from "brandi";
import { GameObject } from "src/core/GameObject";
import { LinearMovementComponent } from "src/core/common-components/MovementComponent";
import { TransformComponent } from "src/core/common-components/TransformComponent";
import { GAME_TOKENS } from "src/di/di.game.tokens";
import { GAMES_CORE_TOKENS } from "src/di/di.games-common.tokens";
import { SpaceShipViewComponent } from "./SpaceShipViewComponent";

export class SpaceShip extends GameObject {
  constructor(
    public readonly transformComponent: TransformComponent,
    public readonly movementComponent: LinearMovementComponent,
    public readonly viewComponent: SpaceShipViewComponent
  ) {
    super();

    this.registerComponent(this.transformComponent, this.movementComponent, this.viewComponent);
  }
}

injected(
  SpaceShip,
  GAMES_CORE_TOKENS.transformComponent,
  GAMES_CORE_TOKENS.linearMovementComponent,
  GAME_TOKENS.gameSpaceShipViewComponent
);
