import { ContainerProvider } from "brandi-react";
import React from "react";
import { createDiRootContainer } from "./di/di.root-container";
import { GamePage } from "./game-page/GamePage";

export function App() {
  return (
    <ContainerProvider container={createDiRootContainer()}>
      <GamePage />
    </ContainerProvider>
  );
}
