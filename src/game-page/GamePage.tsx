import React, { useEffect, useRef, useState } from "react";
import { useAssetsLoader, useGameApplication } from "src/di/di.root-container.hooks";
import "./GamePage.style.scss";

export function GamePage() {
  const gameApplication = useGameApplication();
  const assetsLoader = useAssetsLoader();
  const containerRef = useRef<HTMLDivElement>(null);

  const [isBundleLoaded, setBundleLoaded] = useState(false);

  useEffect(() => {
    assetsLoader.loadBundle().then(() => setBundleLoaded(true));
  }, []);

  useEffect(() => {
    if (containerRef.current && isBundleLoaded && !gameApplication.app) {
      gameApplication.start(containerRef.current);

      return () => {
        gameApplication.destroy();
      };
    }
  }, [isBundleLoaded]);

  return (
    <div className="stage-container">
      <div className="stage-root">
        <div ref={containerRef} className="stage-canvas"></div>
      </div>
    </div>
  );
}
