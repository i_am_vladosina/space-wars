import { EventEmitter } from "@pixi/utils";
import { injected } from "brandi";
import { DI_TOKENS } from "src/di/di.tokens";

// передача данных игры между игровым движком и интерфейсе на html
export class GameDataBridge<Data extends Record<any, any>> {
  public data: Data;
  private readonly eventName = "gameDataChanged";

  constructor(private readonly emitter: EventEmitter) {
    this.data = {} as Data;
  }

  public getData = (): Data => {
    return this.data;
  };

  public setData = (data: Data): void => {
    this.data = data;
  };

  public setValue = <K extends keyof Data>(key: K, value: Data[K]): void => {
    this.data[key] = value;
    this.emitData();
  };

  public subscribeOnDataChanged = (cb: (data: Data) => void): VoidFunction => {
    const handleEmit = (): void => {
      cb(this.data);
    };

    this.emitter.on(this.eventName, handleEmit);

    return () => this.emitter.off(this.eventName, handleEmit);
  };

  private emitData(): void {
    this.data = { ...this.data };
    this.emitter.emit(this.eventName);
  }
}

injected(GameDataBridge, DI_TOKENS.eventEmitter);
