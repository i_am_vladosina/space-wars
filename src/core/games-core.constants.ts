export enum CommonComponentsName {
  View = 'View',
  Transform = 'Transform',
  LinearMovement = 'LinearMovement',
  Health = 'Health',
  Camera = 'Camera',
}
