import { BaseWorld } from "./BaseWorld";
import { IScriptObject } from "./games-core.types";

export abstract class BaseScript implements IScriptObject {
  protected world: BaseWorld;

  public bindWorld(world: BaseWorld): void {
    this.world = world;
  }

  public abstract start(): void;
}
