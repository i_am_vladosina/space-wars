export class GameTime {
  private prevTime: number | null;
  private timeInMs: number;
  private deltaTime: number;

  constructor() {
    this.prevTime = null;
    this.timeInMs = 0;
    this.deltaTime = 0;
  }

  public start(): void {
    // начинаем с той же даты, что и в реальности, пока это ок
    this.prevTime = performance.now();
  }

  public update(): void {
    if (this.prevTime === null) return;

    // мб здесь стоит прибавлять дельту между кадрами из Pixi.Ticker
    const now = performance.now();
    this.deltaTime = now - this.prevTime;
    this.timeInMs += now - this.prevTime;
    this.prevTime = now;
  }

  public stop(): void {
    this.prevTime = null;
  }

  public getTimeInMs(): number {
    return this.timeInMs;
  }

  public getDelta(): number {
    return this.deltaTime;
  }
}
