import { BaseWorld } from "./BaseWorld";
import { IGameObjectComponent } from "./games-core.types";

export class GameObject {
  public tag: string;
  private componentList: IGameObjectComponent[];
  // флаг обозначающий, что этот объект нужно убрать из игры
  public isMarkedAsDestroyed: boolean;
  public world: BaseWorld;

  constructor() {
    this.tag = "untagged";
    this.componentList = [];
    this.isMarkedAsDestroyed = false;
  }

  public registerComponent(...componentList: IGameObjectComponent[]): void {
    this.componentList.push(...componentList);
  }

  public getComponent<T>(component: any): T {
    return this.componentList.find((c) => c instanceof component) as T;
  }

  public getComponentByName<T>(name: string): T | undefined {
    return this.componentList.find((c) => c.name === name) as T | undefined;
  }

  public create(world: BaseWorld): void {
    this.world = world;

    this.componentList.forEach((c) => this.createComponent(c));
  }

  public setAsDestroyed(): void {
    this.isMarkedAsDestroyed = true;
  }

  public update(delta: number, elapsedTime: number): void {
    this.componentList.forEach((c) => c.update?.(delta, elapsedTime));
  }

  public destroy(): void {
    this.componentList.forEach((c) => c.destroy?.());
  }

  public pause(): void {
    this.componentList.forEach((c) => c.pause?.());
  }

  public resume(): void {
    this.componentList.forEach((c) => c.resume?.());
  }

  private createComponent(c: IGameObjectComponent): void {
    c.bindGameObject(this);
    c.create();
  }
}
