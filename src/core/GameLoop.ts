import { injected } from "brandi";
import { Ticker } from "pixi.js";
import { GAMES_CORE_TOKENS } from "src/di/di.games-common.tokens";
import { UTILS_TOKENS } from "src/di/di.utils.tokens";
import { CallbackCollector } from "src/utils/CallbackCollector";
import { BaseWorld } from "./BaseWorld";
import { GameEventsBridge } from "./GameEventsBridge";
import { GameCoreEvents } from "./games-core.types";

export class GameLoop {
  private world: BaseWorld;

  constructor(
    private readonly ticker: Ticker,
    private readonly events: GameEventsBridge<GameCoreEvents>,
    private readonly callbackCollector: CallbackCollector
  ) {
    this.callbackCollector.add(events.subscribeOnEvent(GameCoreEvents.Pause, this.handlePauseEvent));
    this.callbackCollector.add(events.subscribeOnEvent(GameCoreEvents.Resume, this.handleResumeEvent));
  }

  public bindWorld(world: BaseWorld): void {
    this.world = world;
    this.ticker.autoStart = false;
    this.ticker.stop();
  }

  // старт игры
  public start(): void {
    if (this.ticker.started) return;

    this.ticker.add(this.update);
    this.ticker.start();
    this.world.gameTime.start();
    document.addEventListener("visibilitychange", this.handleVisibilityPageChange);
  }

  // возобновление игры
  public resume(): void {
    if (this.ticker.started) return;

    this.ticker.add(this.update);
    this.world.gameTime.start();
    this.world.application.app.start();
    this.world.resume();
  }

  // пауза в игре
  public pause(): void {
    if (!this.ticker.started) return;

    this.ticker.remove(this.update);
    this.world.gameTime.stop();
    this.world.application.app.stop();
    this.world.pause();
  }

  // выход из игры
  public stop(): void {
    this.pause();
    document.removeEventListener("visibilitychange", this.handleVisibilityPageChange);
  }

  public destroy(): void {
    this.stop();
    this.callbackCollector.execute();
  }

  private handleVisibilityPageChange = (): void => {
    if (document.hidden) {
      this.pause();
    } else {
      this.resume();
    }
  };

  private update = (delta: number): void => {
    this.world.gameTime.update();
    this.world.resolveCollisions();
    this.world.clearFromDestroyedObject();
    this.world.update(delta, this.world.gameTime.getDelta());
  };

  private handlePauseEvent = (): void => {
    document.removeEventListener("visibilitychange", this.handleVisibilityPageChange);
    this.pause();
  };

  private handleResumeEvent = (): void => {
    document.addEventListener("visibilitychange", this.handleVisibilityPageChange);
    this.resume();
  };
}

injected(GameLoop, UTILS_TOKENS.ticker, GAMES_CORE_TOKENS.gameEventsBridge, UTILS_TOKENS.callbacksCollector);
