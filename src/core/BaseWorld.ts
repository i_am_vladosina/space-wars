import { Container } from "pixi.js";
import { GameBaseApplication } from "./GameBaseApplication";
import { GameObject } from "./GameObject";
import { GameTime } from "./GameTime";
import { ViewComponent } from "./common-components/ViewComponent";
import { CommonComponentsName } from "./games-core.constants";
import { Dimensions, IScriptObject } from "./games-core.types";

export class BaseWorld {
  public entities: GameObject[];
  public scripts: IScriptObject[];
  public container: Container;
  public worldSize: Dimensions;
  public application: GameBaseApplication<BaseWorld>;

  constructor(public readonly gameTime: GameTime) {
    this.entities = [];
    this.scripts = [];
    this.container = new Container();
  }

  public init(gameApp: GameBaseApplication<BaseWorld>, ...args: any): void {
    this.application = gameApp;
    this.worldSize = gameApp.getStageDimensions();
  }

  public resolveCollisions(): void {}

  public update(delta: number, elapsedTime: number): void {
    this.entities.forEach((e) => e.update(delta, elapsedTime));
    this.scripts.forEach((s) => s.update?.());
  }

  public pause(): void {
    this.entities.forEach((e) => e.pause());
  }

  public resume(): void {
    this.entities.forEach((e) => e.resume());
  }

  public addToWorld(entity: GameObject): void {
    entity.create(this);
    const viewComponent = entity.getComponentByName<ViewComponent>(CommonComponentsName.View);
    if (viewComponent) {
      viewComponent.addToContainer(this.container);
    }
    this.entities.push(entity);
  }

  public removeFromWorld(entity: GameObject): void {
    const index = this.entities.findIndex((e) => e !== entity);
    entity.destroy();
    this.entities.splice(index, 1);
  }

  public clearFromDestroyedObject(): void {
    // создаем копию текущего списка объектов
    // это нужно, тк на destroy может что-то заспавниться в entities
    // поэтому нужно заново собирать массив
    const tempEntities = this.entities.slice();

    // дропаем все, чтобы затем заново заполнить
    // ArrayUtils.clear(this.entities);
    this.entities = [];

    tempEntities.forEach((e) => {
      if (e.isMarkedAsDestroyed) {
        e.destroy();
        return;
      }

      this.entities.push(e);
    });
  }

  public addScript(...scripts: IScriptObject[]): void {
    // todo возможно стоит сделать инициализацию
    scripts.forEach((s) => {
      s.bindWorld(this);
      s.start();
      this.scripts.push(s);
    });
  }

  public getEntitiesCount(instance: any): number {
    return this.entities.filter((e) => e instanceof instance && !e.isMarkedAsDestroyed).length;
  }

  public getEntity<T>(instance: any): T | undefined {
    return this.entities.find((e) => e instanceof instance && !e.isMarkedAsDestroyed) as T | undefined;
  }

  public getEntityByTag<T>(tag: string): T | undefined {
    return this.entities.find((e) => e.tag === tag && !e.isMarkedAsDestroyed) as T | undefined;
  }

  public getEntityListByTag<T>(tag: string): T[] {
    return this.entities.filter((e) => e.tag === tag && !e.isMarkedAsDestroyed) as T[];
  }

  public getEntitiesCountByTag(tag: string): number {
    return this.entities.filter((e) => e.tag === tag && !e.isMarkedAsDestroyed).length;
  }

  public getScript<T>(script: any): T | undefined {
    return this.scripts.find((s) => s instanceof script) as T | undefined;
  }

  public destroy(): void {
    this.entities.forEach((e) => this.removeFromWorld(e));
    this.scripts.forEach((s) => s.destroy?.());
  }

  public getEntityList<T>(gameObject: any): T[] {
    return this.entities.filter((e) => e instanceof gameObject && !e.isMarkedAsDestroyed) as T[];
  }
}
