import { BaseWorld } from "./BaseWorld";
import { GameObject } from "./GameObject";

export interface IScriptObject {
  bindWorld(world: BaseWorld): void;
  start(): void; // инициализация скрипта до старта геймлупа
  update?(): void; // обновление в геймлупе
  destroy?(): void; // отписки и прочее
}

export interface Dimensions {
  width: number;
  height: number;
}

export interface IGameObjectComponent {
  name: string;
  gameObject: GameObject;
  // привязка компонента к своему игровому объекту
  bindGameObject(g: GameObject): void;
  // создание игрового объекта (вызывается на добавление в мир)
  create(): void;
  // обновление компонента на requestAnimationFrame
  update?(delta: number, elapsedTime: number): void;
  pause?(): void;
  resume?(): void;
  // удаление компонента
  destroy?(): void;
}

export interface ICollisionsDetector {
  create(world: BaseWorld): void;
  resolveCollisions(): void;
}

export enum GameCoreEvents {
  GameStart = "Start",
  Pause = "Pause",
  Resume = "Resume",
  GameEnd = "GameEnd",
  GameInitialized = "GameInitialized",
}
