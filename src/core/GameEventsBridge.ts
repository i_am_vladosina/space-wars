import { EventEmitter } from "@pixi/utils";
import { injected } from "brandi";
import { UTILS_TOKENS } from "src/di/di.utils.tokens";

export class GameEventsBridge<Events extends string> {
  constructor(private readonly emitter: EventEmitter) {}

  public emitEvent(event: Events): void {
    this.emitter.emit(event);
  }

  public subscribeOnEvent = (event: Events, cb: VoidFunction): VoidFunction => {
    this.emitter.on(event, cb);

    return () => this.emitter.off(event, cb);
  };
}

injected(GameEventsBridge, UTILS_TOKENS.eventEmitter);
