import { Circle, Container, Point, Rectangle } from "pixi.js";
import { CommonComponentsName } from "../games-core.constants";
import { IGameObjectComponent } from "../games-core.types";
import { GameObjectComponent } from "./GameObjectComponent";
import { TransformComponent } from "./TransformComponent";

export enum ViewComponentShapeType {
  Rectangle = "Rectangle",
  // у окружности, нарисованной с помощью Graphics.drawRect
  // центр координат находится в центр окружности
  GraphicsCircle = "GraphicsCircle",
  // а у спрайтовой окружности центр координат в левом верхнем углу
  SpriteCircle = "SpriteCircle",
}

export abstract class ViewComponent<View extends Container = Container>
  extends GameObjectComponent
  implements IGameObjectComponent
{
  public view: View;
  public name: string;
  protected transformComponent: TransformComponent;
  public shapeType: ViewComponentShapeType = ViewComponentShapeType.Rectangle;

  public create(): void {
    this.view = this.createView();

    this.name = CommonComponentsName.View;
    this.transformComponent = this.gameObject.getComponent(TransformComponent);

    this.transformComponent.position = this.view.position;
    this.transformComponent.scale = this.view.scale;
    this.transformComponent.rotation = this.view.rotation;
  }

  public destroy(): void {
    this.view.destroy();
  }

  get center(): Point {
    return new Point(
      this.transformComponent.position.x + (0.5 - this.anchor.x) * this.width,
      this.transformComponent.position.y + (0.5 - this.anchor.y) * this.height
    );
  }

  // прямоугольник коллизий объекта
  get intersectRectBounds(): Rectangle {
    return new Rectangle(
      this.transformComponent.position.x - this.anchor.x * this.width,
      this.transformComponent.position.y - this.anchor.y * this.height,
      this.width,
      this.height
    );
  }

  // окружность коллизий объекта
  get intersectCircleBounds(): Circle {
    // (в коллизиях используется центр окружности, поэтому передаем его)
    if (this.isSpriteCircle()) {
      return new Circle(
        this.transformComponent.position.x + (0.5 - this.anchor.x) * this.width,
        this.transformComponent.position.y + (0.5 - this.anchor.y) * this.height,
        this.width / 2
      );
    }

    return new Circle(this.transformComponent.position.x, this.transformComponent.position.y, this.width / 2);
  }

  get bounds(): Rectangle {
    const x = this.transformComponent.position.x - this.anchor.x * this.width;
    const y = this.transformComponent.position.y - this.anchor.y * this.height;

    return new Rectangle(x, y, x + this.width, y + this.height);
  }

  set width(w: number) {
    this.view.width = w;
  }

  get width(): number {
    return this.view.width;
  }

  get halfWidth(): number {
    return this.view.width / 2;
  }

  get height(): number {
    return this.view.height;
  }

  get halfHeight(): number {
    return this.view.height / 2;
  }

  set height(h: number) {
    this.view.height = h;
  }

  set scale(v: number) {
    this.view.scale.set(v);
  }

  get anchor(): Point {
    return "anchor" in this.view ? (this.view.anchor as Point) : new Point();
  }

  public addToContainer(container: Container): void {
    container.addChild(this.view);
  }

  public isSpriteCircle(): boolean {
    return this.shapeType === ViewComponentShapeType.SpriteCircle;
  }

  public isRectangle(): boolean {
    return this.shapeType === ViewComponentShapeType.Rectangle;
  }

  public isGraphicsCircle(): boolean {
    return this.shapeType === ViewComponentShapeType.GraphicsCircle;
  }

  abstract createView(): View;
}
