import {CommonComponentsName} from '../games-core.constants';

import {GameObjectComponent} from './GameObjectComponent';

export class HealthComponent extends GameObjectComponent {
  public healthValue: number;

  public setHealth(health: number): void {
    this.healthValue = health;
  }

  public create(): void {
    this.name = CommonComponentsName.Health;
  }

  public decreaseLife(): void {
    this.healthValue = Math.max(this.healthValue - 1, 0);
  }

  public isDead(): boolean {
    return this.healthValue === 0;
  }
}
