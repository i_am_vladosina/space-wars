import { GameObjectComponent } from "src/core/common-components/GameObjectComponent";

export class TimeStopwatchComponent extends GameObjectComponent implements GameObjectComponent {
  private timeInMs: number;
  private isStarted: boolean;

  public create(): void {
    this.timeInMs = 0;
    this.isStarted = false;
  }

  // старт таймера
  public start(): void {
    this.isStarted = true;
  }

  // остановка текущего отсчета
  public stop(): void {
    this.isStarted = false;
  }

  // сброс текущего отсчета
  public reset(): void {
    this.timeInMs = 0;
  }

  public update(delta: number, elapsedTime: number): void {
    if (!this.isStarted) return;

    this.timeInMs += elapsedTime;
  }

  public getTimeInMs(): number {
    return this.timeInMs;
  }
}
