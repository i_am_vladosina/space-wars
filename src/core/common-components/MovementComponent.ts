import {CommonComponentsName} from '../games-core.constants';
import {IGameObjectComponent} from '../games-core.types';
import {Point} from 'pixi.js';

import {GameObjectComponent} from './GameObjectComponent';
import {TransformComponent} from './TransformComponent';

export class LinearMovementComponent extends GameObjectComponent implements IGameObjectComponent {
  public velocity: number;
  public direction: Point;

  private transformComponent: TransformComponent;

  public create(): void {
    this.name = CommonComponentsName.LinearMovement;
    this.transformComponent = this.gameObject.getComponent(TransformComponent);

    this.velocity = 0;
    this.direction = new Point();
  }

  public update(delta: number): void {
    this.transformComponent.position.x += this.direction.x * this.velocity * delta;
    this.transformComponent.position.y += this.direction.y * this.velocity * delta;
  }
}
