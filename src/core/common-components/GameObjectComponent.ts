import { GameObject } from "src/core/GameObject";
import { IGameObjectComponent } from "../games-core.types";

export abstract class GameObjectComponent<G extends GameObject = GameObject> implements IGameObjectComponent {
  public gameObject: G;
  public name: string = "";

  public bindGameObject(g: G): void {
    this.gameObject = g;
  }

  abstract create(): void;
}
