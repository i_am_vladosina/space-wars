import {CommonComponentsName} from '../games-core.constants';
import {IGameObjectComponent} from '../games-core.types';
import {Point} from 'pixi.js';

import {GameObjectComponent} from './GameObjectComponent';

export class TransformComponent extends GameObjectComponent implements IGameObjectComponent {
  public position: Point;
  public scale: Point;
  public rotation: number;

  public create(): void {
    this.name = CommonComponentsName.Transform;
    this.position = new Point();
    this.scale = new Point();
    this.rotation = 0;
  }

  set positionFromPoint(p: Point) {
    this.position.x = p.x;
    this.position.y = p.y;
  }
}
