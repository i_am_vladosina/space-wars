import {CommonComponentsName} from '../games-core.constants';
import {Container, Rectangle} from 'pixi.js';

import {GameObjectComponent} from './GameObjectComponent';
import {TransformComponent} from './TransformComponent';

export interface CameraSettings {
  target: TransformComponent;
  world: Container;
  screenSize: Rectangle;
}

export class CameraComponent extends GameObjectComponent {
  private target: TransformComponent;
  private world: Container;
  private centerScreenPointX: number;
  private centerScreenPointY: number;
  private rightBorderWorldPointX: number;
  private heightBorderWorldPointY: number;

  public create(): void {
    this.name = CommonComponentsName.Camera;
    this.target = this.gameObject.getComponent(TransformComponent);
    this.world = this.gameObject.world.container;

    this.centerScreenPointX = this.gameObject.world.worldSize.width / 2;
    this.centerScreenPointY = this.gameObject.world.worldSize.height / 2;
    this.rightBorderWorldPointX = this.world.width - this.centerScreenPointX;
    this.heightBorderWorldPointY = this.world.height - this.centerScreenPointY;
  }

  public update(): void {
    if (this.target.position.x > this.centerScreenPointX && this.target.position.x < this.rightBorderWorldPointX) {
      this.world.x = this.centerScreenPointX - this.target.position.x;
    }
    if (this.target.position.y > this.centerScreenPointY && this.target.position.y < this.heightBorderWorldPointY) {
      this.world.y = this.centerScreenPointY - this.target.position.y;
    }
  }
}
