import { GameObjectComponent } from "src/core/common-components/GameObjectComponent";

export class CountdownComponent extends GameObjectComponent {
  public countdownTimeInMs: number;
  private isStarted: boolean;

  public setTime(timeInMs: number): void {
    this.countdownTimeInMs = timeInMs;
  }

  public create(): void {
    this.isStarted = false;
  }

  // старт таймера
  public start(): void {
    this.isStarted = true;
  }

  // остановка текущего отсчета
  public stop(): void {
    this.isStarted = false;
  }

  public update(delta: number, elapsedTime: number): void {
    if (this.countdownTimeInMs === 0 || !this.isStarted) return;

    this.countdownTimeInMs = Math.max(0, this.countdownTimeInMs - elapsedTime);
  }
}
