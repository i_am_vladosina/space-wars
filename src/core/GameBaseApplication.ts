import { Application, Color, Container } from "pixi.js";
import { CallbackCollector } from "src/utils/CallbackCollector";
import { BaseWorld } from "./BaseWorld";
import { GameEventsBridge } from "./GameEventsBridge";
import { GameLoop } from "./GameLoop";
import { GameCoreEvents } from "./games-core.types";

export interface AppOptions {
  pixiApp: {
    backgroundAlpha: number;
    backgroundColor: Color;
  };
}

export class GameBaseApplication<W extends BaseWorld> {
  public app: Application;
  protected containerNode: HTMLElement;
  protected rootContainer: Container;

  constructor(
    private readonly options: AppOptions,
    protected readonly world: W,
    private readonly gameEvents: GameEventsBridge<GameCoreEvents>,
    private readonly callbackCollector: CallbackCollector,
    public readonly gameLoop: GameLoop
  ) {}

  public start(containerNode: HTMLElement, ...args: any): void {
    this.containerNode = containerNode;
    const { width, height } = this.containerNode.getBoundingClientRect();

    this.app = new Application({
      width: width,
      height: height,
      antialias: true,
      resolution: this.getAppDPR(),
      backgroundAlpha: this.options.pixiApp.backgroundAlpha,
      backgroundColor: this.options.pixiApp.backgroundColor,
      autoDensity: true,
    });

    const canvas = this.app.view as HTMLCanvasElement;
    this.containerNode.appendChild(canvas);
    canvas.id = "game-canvas";

    canvas.style.width = "100%";
    canvas.style.height = "100%";

    // это нужно для локального использования браузерного расширения для пикси
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (globalThis as any).__PIXI_APP__ = this.app;

    this.rootContainer = new Container();
    this.app.stage.addChild(this.rootContainer);

    this.callbackCollector.add(this.gameEvents.subscribeOnEvent(GameCoreEvents.Pause, this.handleGamePause));
    this.callbackCollector.add(this.gameEvents.subscribeOnEvent(GameCoreEvents.Resume, this.handleGameResume));
    this.callbackCollector.add(this.gameEvents.subscribeOnEvent(GameCoreEvents.GameStart, this.handleGameStart));
    this.callbackCollector.add(this.gameEvents.subscribeOnEvent(GameCoreEvents.GameEnd, this.handleGameEnd));
    this.callbackCollector.add(
      this.gameEvents.subscribeOnEvent(GameCoreEvents.GameInitialized, this.handleGameInitialized)
    );

    this.rootContainer.addChild(this.world.container);
    this.gameLoop.bindWorld(this.world);
    this.world.init(this);
  }

  public destroy(): void {
    this.app.destroy();
    this.callbackCollector.execute();
    this.world.destroy();
  }

  public getAppDPR(): number {
    return window.devicePixelRatio;
  }

  public getStageDimensions(): { width: number; height: number } {
    const renderer = this.app.renderer;
    const dpr = this.getAppDPR();

    return {
      width: renderer.width / dpr,
      height: renderer.height / dpr,
    };
  }

  private handleGamePause = (): void => {
    this.app.stage.eventMode = "none";
  };

  private handleGameResume = (): void => {
    this.app.stage.eventMode = "auto";
  };

  private handleGameStart = (): void => {
    this.gameLoop.start();
  };

  private handleGameEnd = (): void => {
    this.gameLoop.stop();
  };

  private handleGameInitialized = (): void => {
    this.gameLoop.start();
  };
}

// injected(GameApplication, GAMES_CORE_TOKENS., GAMES_CORE_TOKENS.w)

// private readonly options: AppOptions,
// protected readonly world: W,
// private readonly gameEvents: GameEventsBridge<GameCoreEvents>,
// private readonly callbackCollector: CallbackCollector,
// public readonly gameLoop: GameLoop
